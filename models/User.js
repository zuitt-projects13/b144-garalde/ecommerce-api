const mongoose = require("mongoose")

const userSchema = new mongoose.Schema({

  isAdmin:{
    type: Boolean,
    default: false
  },
  firstName: {
    type:String,
    required:[true,"First Name is required."]
  },
  lastName:{
    type:String,
    required:[true,"Last Name is required."]
  },
  age:{
    type:String,
    required:[true,"Age is required."]
  },
  gender:{
    type:String,
    required:[true,"Gender is required."]
  },
  email:{
    type:String,
    required:[true,"Email is required."]
  },
  password:{
    type:String,
    required:[true,"Password is required."]
  },
  mobileNo:{
    type:String,
    required:[true,"Mobile number is required."]
  },
  deliveryAddress:{
    type:String,
    required:[true,"Delivery address is required."]
  },
  cart: [
      {
        productId:{
          type:String,
          required:[true,"Product ID is required."]
        },
        productName: {
          type:String,
          required:[true,"Product name is required"]
        },
        productCategory:{
          type:String,
          required:[true,"Product category is required"]
        },
        productVolume:{
          type:String,
          required:[true,"Product volume required"]
        },
        productPrice: {
          type:Number,
          required:[true,"Product Price is required."]
        },
        productQty: {
          type:Number,
          required:[true,"Product Qty is required."]
        },
        orderId:{
          type:String,
          default: ''
        },
        isCheckedOut: {
          type:Boolean,
          default: false
        },
        isProcessed: {
          type:Boolean,
          default: false
        }
        
      }//end of object
    ]

})


module.exports = mongoose.model("User",userSchema)