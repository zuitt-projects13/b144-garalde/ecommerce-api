const express = require("express")
const router = express.Router()

const userController = require("../controllers/user")

const auth = require("../auth")


// user registration
router.post("/register",(request,response) => {
	userController.registerUser(request.body).then(result => response.send(result))
})

router.post('/checkEmail', (req, res)=>{
	userController.checkEmailExists(req.body).then(result => res.send(result))
})

router.post("/login", (request,response) => {
	userController.loginUser(request.body).then(result => response.send(result))
})


router.put("/:id/setAsAdmin", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	if (userData.isAdmin){
		userController.setAdmin(request.params.id,userData.isAdmin,request.body).then(result => response.send(result))
	}
	else {
		response.send("Please use an admin account")
	}
})

router.put("/changePassword", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	userController.changePassword(userData.id,request.body).then(result => response.send(result))
})

router.put("/updateInfo",auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	userController.updateInfo(userData.id,request.body).then(result => response.send(result))
})

router.get("/details", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	// Provides the user's ID for the getProfile controller method
	userController.getProfile({userId : userData.id}).then(resultFromController => res.send(resultFromController));

});

// add to cart
router.post("/addToCart", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	let data = {
		userId : userData.id,
		productId : request.body.productId,
		productQty : request.body.productQty

	}

	
	if (!userData.isAdmin){
		userController.addToCart(data).then(result => response.send(result))
	}
	else {
		response.send("Add to cart option is for non admin account only.")
	}
})


// checkout
router.post("/checkout", auth.verify, (request,response) => {
	let userData = auth.decode(request.headers.authorization)

	if (!userData.isAdmin){
		userController.checkout(userData.id).then(result => response.send(result))
	}
	else {
		response.send("Checkout option is for non admin account only.")
	}

})



router.put("/updateQty", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	if (userData.isAdmin === false){
		userController.updateQty(userData.id,request.body).then(result => response.send(result))
	}
	else {
		response.send("Please use a non admin account")
	}
})




router.put("/removeItem", auth.verify, (request,response) => {
	const userData = auth.decode(request.headers.authorization)
	if (userData.isAdmin === false){
		userController.removeItem(userData.id,request.body.index).then(result => response.send(result))
	}
	else {
		response.send("Please use a non admin account")
	}
})


module.exports = router;