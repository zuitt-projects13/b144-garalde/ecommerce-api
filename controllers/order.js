const User = require("../models/User")
const Product = require("../models/Product")
const Order = require("../models/Order")

const bcrypt = require("bcrypt")


const auth = require("../auth")



allOrders = () => {
	return Order.find().then((result,error) => {
		if (error) {
			return false
		}
		else {
			return result
		}
	})
}


userOrders = (userId) => {
	return Order.find({userId : userId}).then((result,error) => {
		if (error) {
			return false
		}
		else {
			return result
		}
	})
}




processOrder = async (data) => {

	let updatedProcess = await Order.findById(data.orderId).then((result,error) => {
						if (error){
							return false
						}
						else {
							result.isProcessed = true
							result.deliveryStatus = "For Delivery"
							return result.save().then((result,error) => {
								if(error) {
									return false
								}
								else {
									return true
								}
							})
						}
					})


	if (updatedProcess) {
		return User.findById(data.userId).then(user => {

				let cart=user.cart

				cart.forEach((order,idx) => {
					if (order.orderId === data.orderId){
						console.log(`${idx} ${order.orderId}`)
						user.cart[idx].isProcessed=true
						user.save().then((result,error) => {
								if(error) {
									return false
								}
							})
					}
					
				})
				return true

			})
	}
}


orderDetails = (orderId) => { 
	return Order.findById(orderId).then((result,error) => {
		if(error) {
			return false
		}
		else {
			return result
		}
	})
}


module.exports = {
	allOrders,
	userOrders,
	processOrder,
	orderDetails
}