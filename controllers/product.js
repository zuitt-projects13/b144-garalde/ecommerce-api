const Product = require("../models/Product")

const auth = require("../auth")


addProduct = (requestBody, isAdmin) => {

	let newProduct = new Product({
		productName: requestBody.productName,
		productCategory:requestBody.productCategory,
		productVolume:requestBody.productVolume,
		productDescription: requestBody.productDescription,
		productPrice: requestBody.productPrice

	})

	return newProduct.save().then((result,error) => {
		if (error){
			return false
		}
		else {
			return result
		}
	})

}


allProduct = () => {
	return Product.find().then((result,error) => {
		if(error){
			return false
		}
		else {
			return result
		}
	})
}


activeProduct = () => {
	return Product.find({ isActive : true}).then((result,error) => {
		if(error){
			return false
		}
		else {
			return result
		}
	})
}



getProduct = (productID) => {
	return Product.findById(productID).then((result,error) => {
		if(error) {
			return false
		}
		else {
			return result
		}
	})
}



updateProduct = (productID, requestBody) => {

	let updatedProduct = {
		productName,
		productCategory,
		productVolume,
		productDescription,
		productPrice
	} = requestBody

	return Product.findByIdAndUpdate(productID, updatedProduct).then((result,error) => {
		if (error) {
			return false
		}
		else {

			return true
		}
	})

}



archiveProduct = (productID) => {
	return Product.findByIdAndUpdate(productID, { isActive : false}).then((result,error) => {
		if (error){
			return false
		}
		else {
			return true
		}
	})
}

activateProduct = (productID) => {
	return Product.findByIdAndUpdate(productID, { isActive : true}).then((result,error) => {
		if (error){
			return false
		}
		else {
			return true
		}
	})
}


module.exports = {
	addProduct,
	allProduct,
	activeProduct,
	getProduct,
	updateProduct,
	archiveProduct,
	activateProduct
}