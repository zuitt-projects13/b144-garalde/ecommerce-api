const jwt = require("jsonwebtoken")
require('dotenv').config()
const secret = process.env.DB_SECRET


createAccessToken = (user) => {
	const data = {
		id : user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	return jwt.sign(data,secret,{})

}



verify = (request,response,next) => {
	let token = request.headers.authorization

	if (typeof token !== "undefined"){
		console.log(token)
		token=token.slice(7,token.length)

		return jwt.sign(token,secret, (error,data) => {
			if (error) {
				return response.send({auth : "Unauthorized User"}) 
			}
			else {
				next()
			}
		})
	}
	else {
		return response.send({auth : "Authentication Failed."}) 
	}

}




decode = (token) => {

	if (typeof token !== "undefined"){

		token=token.slice(7,token.length)

		return jwt.verify(token,secret, (error,data) => {
			if (error) {
				return null 
			}
			else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else {
		return null 
	}

}

module.exports = {
	createAccessToken,
	verify,
	decode
}