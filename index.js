// set up dependencies
const express = require("express")
const mongoose = require("mongoose")
const cors = require("cors")
require('dotenv').config()

// Route Connection
const userRoutes = require("./routes/user")
const productRoutes = require("./routes/product")
const orderRoutes = require("./routes/order")

// end of Route connection


// server setup
const app = express()
const port = 4000;

// all all resources to access backend
// enable all cors request
app.use(cors())



app.use(express.json())
app.use(express.urlencoded({extended :true}))



// set route endpoint
app.use("/api/users", userRoutes)
app.use("/api/products", productRoutes)
app.use("/api/orders", orderRoutes)

// end of route endpoint




// mongoDB Connection
const databaseUrl = process.env.DB_HOST
mongoose.connect(databaseUrl,
{
	useNewUrlParser:true,
	useUnifiedTopology:true
})



// checking if connection with mongoDB is successful
let db = mongoose.connection;

db.on("error",console.log.bind(console,"connection error"))
db.once("open", () => console.log("Were connected to the cloud database"))

// mongoDB Connection end



app.listen(process.env.PORT || port, () => console.log(`Now listening to port ${process.env.PORT || port}`))